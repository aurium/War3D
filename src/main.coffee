globeRotation = 0
globeRotationInc = 0
globeRollIntention = 0
globeRoll = 0
{ abs, sign } = Math
rootTwo = Math.sqrt 2
camZIntention = 999
camZ = 999
month = 1
origMapWidth = 5400
origMapHeight = 2700

twoDig = (num)-> if num < 10 then '0' + num else num.toString()

shuffle = (array)->
  for currentIndex in [array.length-1..1]
    randomIndex = Math.floor Math.random() * currentIndex
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  array

updateStatus = ->
    do updateGlobeRotation
    do updateGlobeRoll
    do updateCam
setInterval updateStatus, 50

monthPics = []
do ->
    ctx = globeSkin.getContext '2d'
    loadedMonths = 0
    for month in [1..12]
        img = monthPics[month] = document.createElement 'img'
        assets.appendChild img
        img.id = "globe-month-#{month}"
        img.monthNum = month
        img.width = 90
        img.onload = ->
            console.log "Loaded globe pic for month #{this.monthNum}"
            ctx.drawImage this, 0, 0, origMapWidth, origMapHeight, 0, 0, globeSkin.width, globeSkin.height
            this.imgData = ctx.getImageData 0, 0, globeSkin.width, globeSkin.height
            do initGlobe if ++loadedMonths is 12
            loading.style.width = (100 * 12/loadedMonths) + '%'
            this.parentNode.removeChild this
        img.onerror = (err)->
            console.error "Fail to load #{this.id}.", err
            alert "Fail to load #{this.id}."
        img.src = "world.topo.bathy.2004#{twoDig month}.3x5400x2700.jpg"

monthTransStep = 0
monthTransStepLen = 30
transRand = (shuffle [0..monthTransStepLen-1] for [0..99])

initGlobe = ->
    setTimeout (-> loadingBar.parentNode.removeChild loadingBar), 100
    console.log 'Init Globe'
    w = globeSkin.width
    h = globeSkin.height
    ctx = globeSkin.getContext '2d'
    ctx.putImageData monthPics[1].imgData, 0, 0
    globe.setAttribute 'material', "src:"
    globe.setAttribute 'material', "src:#globeSkin"
    tempData = ctx.getImageData 0, 0, w, h
    camZIntention = 16
    setInterval (->
        start1 = Date.now()
        if monthTransStep is monthTransStepLen
            monthTransStep = 0
            month++
        month = 1 if month > 12
        imgData = monthPics[month].imgData.data
        start2 = Date.now()
        for i in [0..(w*h)-monthTransStepLen] by monthTransStepLen
            pos = (i + transRand[(i/monthTransStepLen)%transRand.length][monthTransStep]) * 4
            pos = (w*h*4)-1 if pos >= (w*h*4)
            tempData.data[pos] = imgData[pos]
            tempData.data[pos+1] = imgData[pos+1]
            tempData.data[pos+2] = imgData[pos+2]
            tempData.data[pos+3] = imgData[pos+3]
        #console.log 'Pix copy delay:', Date.now() - start2
        ctx.putImageData tempData, 0, 0
        console.log "Month #{month}"
        globe.setAttribute 'material', "src:"
        globe.setAttribute 'material', "src:#globeSkin"
        monthTransStep++
        #console.log 'Frame build delay:', Date.now() - start1
    ), 500

mkCloudSkin = ->
    console.log "Loaded clouds pic."
    w = cloudSkin.width
    h = cloudSkin.height
    ctx = cloudSkin.getContext '2d'
    ctx.drawImage cloudImg, 0, 0, w, h
    imgData = ctx.getImageData 0, 0, w, h
    for i in [0..(w*h)-1]
        imgData.data[i*4+3] = imgData.data[i*4]
        imgData.data[i*4+0] = 255
        imgData.data[i*4+1] = 255
        imgData.data[i*4+2] = 255
    ctx.putImageData imgData, 0, 0
    clouds.setAttribute 'opacity', 1
    clouds.setAttribute 'material', src: '#cloudSkin'
cloudImg = document.createElement 'img'
assets.appendChild cloudImg
cloudImg.onload = mkCloudSkin
cloudImg.width = 90
cloudImg.src = "cloud.jpg"

updateGlobeRotation = ->
    globeRotation += globeRotationInc
    globe.setAttribute 'rotation', "0 #{globeRotation} 0"

updateGlobeRoll = ->
    rollDelta = globeRollIntention - globeRoll
    globeRoll += if abs(rollDelta) > 3 then rollDelta/2 else \
                 if abs(rollDelta) < 1 then rollDelta else sign(rollDelta)
    globeWrap.setAttribute 'rotation', "#{globeRoll} 0 0"

updateCam = ->
    camZDelta = camZIntention - camZ
    camZ += if abs(camZDelta) > 3.0 then camZDelta/2 else \
            if abs(camZDelta) < 0.5 then camZDelta else sign(camZDelta)/2
    cam.setAttribute 'position', "0 0 #{camZ}"

document.body.addEventListener 'mousemove', (ev)=>
    globeRotationInc = ((ev.clientX / document.body.clientWidth) * -4 + 2) ** 3
    globeRollIntention = (ev.clientY / document.body.clientHeight) * -240 + 120
    if abs(globeRollIntention) > 90
        globeRollIntention = 90 * sign(globeRollIntention)

document.body.addEventListener 'keydown', (ev)=>
    if ev.key in ['+', '=', 'ArrowUp']
        camZIntention--
    if ev.key in ['-', 'ArrowDown']
        camZIntention++
    camZIntention = 12 if camZIntention < 12
    camZIntention = 30 if camZIntention > 30

