War 3D
======

A classic game in 3D.

## Build

```bash
$ npm install
$ npm run build
```

## Serve and play

```bash
$ npm start
```
...then open http://localhost:8000 in your web browser.
